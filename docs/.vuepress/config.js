module.exports = {
  title: 'ICT Studie NL',
  description: 'De handleiding voor VISTA College.',
  dest: 'public/',
  themeConfig: {
    // logo: '/vuepress-logo.png',
    lastUpdated: 'Last updated',
    repo: 'https://gitlab.com/vistacollege/ict-studie-nl',
    docsDir: 'docs',
    editLinks: true,
    editLinkText: 'Recommend a change',
    nav: [
      {
        text: 'Home',
        link: '/'
      },
      {
        text: 'Handleiding',
        link: '/manual/'
      },
      {
        text: 'Contact',
        items: [
          {
            text: 'Twitter',
            link: 'https://www.twitter.com/@TCOOfficiall'
          },
          {
            text: 'Email',
            link: 'mailto:405335@vistacollege.nl'
          }
        ]
      }
    ],
    plugins: ['@vuepress/active-header-links']
  }
}
